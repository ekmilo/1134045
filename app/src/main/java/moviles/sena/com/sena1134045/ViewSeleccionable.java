package moviles.sena.com.sena1134045;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import moviles.sena.com.sena1134045.adapter.MovieAdapter;

public class ViewSeleccionable extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_seleccionable);

        ListView lista = (ListView)findViewById(R.id.listview);
        Spinner spinner = (Spinner)findViewById(R.id.spinner);
        AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView)
                findViewById(R.id.autcomplete);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("view_seleccionable", "selccionó el item de la pos " + position);

                String [] arreglo = getResources().getStringArray(R.array.series);
                Log.d("view_seleccionable", "seleccionó " + arreglo[position]);

            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        String [] tipodocumento={"cedula","tarjeta","registro"};
        List<String> listaitems=new ArrayList<String>();
        listaitems.add("algo");listaitems.add("algo1");listaitems.add("algo2");

        //ArrayAdapter simple:
        ArrayAdapter adapter = new ArrayAdapter(ViewSeleccionable.this,
                android.R.layout.simple_dropdown_item_1line,tipodocumento);
        autoCompleteTextView.setAdapter(adapter);
        //ArrayAdapter personalizado
        ArrayAdapter adapterArrayPersonalizado = new ArrayAdapter(ViewSeleccionable.this,
               R.layout.itempeliculas ,R.id.text1,
                getResources().getStringArray(R.array.series));

        lista.setAdapter(adapterArrayPersonalizado);
        spinner.setAdapter(adapterArrayPersonalizado);

        //setadapter adaptador propio
        MovieAdapter movieAdapter = new MovieAdapter(ViewSeleccionable.this);
        lista.setAdapter(movieAdapter);

        //Gridview
        GridView gridView = (GridView)findViewById(R.id.gridview);
        gridView.setAdapter(movieAdapter);

    }
}
