package moviles.sena.com.sena1134045;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;

public class CameraActivity extends AppCompatActivity {

    ImageView foto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);


        foto=(ImageView)findViewById(R.id.image);
        ImageButton imageButton=(ImageButton)findViewById(R.id.button);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,0);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 0){
          Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            foto.setImageBitmap(bitmap);

          Animation animation= AnimationUtils.loadAnimation(CameraActivity.this,
                    R.anim.chiquitita);
          foto.setAnimation(animation);
          //animation.startNow();

        }

    }
}
