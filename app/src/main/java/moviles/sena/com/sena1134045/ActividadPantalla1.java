package moviles.sena.com.sena1134045;

import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class ActividadPantalla1 extends AppCompatActivity {

    EditText editText;
    public final static String clave ="texto";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_pantalla1);

        editText = (EditText)findViewById(R.id.text);
    }

    public void hacerClick(View v){
        Intent intent = new Intent(this,ActividadPantalla2.class);

        intent.putExtra(clave,editText.getText().toString());

        Bundle contenedor = new Bundle();
        contenedor.putString(clave,editText.getText().toString());
        intent.putExtras(contenedor);

        startActivity(intent);
    }

    public void llamarPorResultado(View v){

        Intent intent = new Intent(this,ActividadPantalla2.class);

        intent.putExtra(clave,editText.getText().toString());
        Bundle contenedor = new Bundle();
        contenedor.putString(clave,editText.getText().toString());
        intent.putExtras(contenedor);
        startActivityForResult(intent,0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
     //   super.onActivityResult(requestCode, resultCode, data);
       if(requestCode == 0){
              if(resultCode == 1){
                  String valor= data.getStringExtra(ActividadPantalla2.nombre);
                  Toast.makeText(this, "resultado " + valor, Toast.LENGTH_LONG).show();

              }
       }



    }
}
