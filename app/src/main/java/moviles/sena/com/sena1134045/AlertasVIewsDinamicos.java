package moviles.sena.com.sena1134045;

import android.app.Dialog;
import android.content.DialogInterface;
import android.media.Image;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class AlertasVIewsDinamicos extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alertas_views_dinamicos);

        Button button=(Button)findViewById(R.id.button);
        final EditText editText=(EditText)findViewById(R.id.edittexto);
        final LinearLayout contenedor = (LinearLayout)findViewById(R.id.contenedor);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView texto = new TextView(AlertasVIewsDinamicos.this);
                texto.setTextSize(30);
                texto.setTextColor(getResources().getColor(R.color.colorAccent));
                texto.setText(editText.getText().toString());
                ImageView imageView = new ImageView(AlertasVIewsDinamicos.this);
                Picasso.with(AlertasVIewsDinamicos.this).load(R.drawable.angrybirds).resize(200,200).into(imageView);
              /*  imageView.setImageDrawable(getResources().getDrawable(R.drawable.angrybirds));
                imageView.setLayoutParams(new ViewGroup.LayoutParams(200,200));
                imageView.setMinimumWidth(200);
                imageView.setMinimumHeight(200);
                */
                contenedor.addView(imageView);
                contenedor.addView(texto);

                //Crear Toast:
                Toast.makeText(AlertasVIewsDinamicos.this,"Soy un toast",Toast.LENGTH_LONG).show();
                //Alertas:
                AlertDialog.Builder constructor = new AlertDialog.Builder(AlertasVIewsDinamicos.this);
                constructor.setTitle("Título");
                constructor.setMessage("Mensaje de alerta");
                constructor.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(AlertasVIewsDinamicos.this,"ok",Toast.LENGTH_LONG).show();

                    }
                });
                constructor.setNegativeButton("cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(AlertasVIewsDinamicos.this,"Canceló",Toast.LENGTH_LONG).show();

                    }
                });
                constructor.show();

            }
        });

    }
}
