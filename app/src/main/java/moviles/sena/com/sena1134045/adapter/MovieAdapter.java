package moviles.sena.com.sena1134045.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import moviles.sena.com.sena1134045.R;
import moviles.sena.com.sena1134045.entity.Movie;

/**
 * Created by ekmil on 16/5/2016.
 */
public class MovieAdapter extends BaseAdapter {

    List<Movie> listaPeliculas;
    Context context;
    public MovieAdapter(Context context){
        listaPeliculas=Movie.obtenerListaPeliculas();

        this.context=context;
    }

    @Override
    public int getCount() {
        return listaPeliculas.size();
    }

    @Override
    public Object getItem(int position) {
        return listaPeliculas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vista;
        if(convertView != null){
            vista=convertView;
        }else{
           //Inflar una vista => inflate
          vista= View.inflate(context, R.layout.itempeliculaspersonalizado, null);
        }
        //Llenar los datos del item o vista
        TextView textView=(TextView)vista.findViewById(R.id.texto);
        textView.setText(listaPeliculas.get(position).getName());

        ImageView imageView=(ImageView)vista.findViewById(R.id.imagen);
        Picasso.with(context).load(listaPeliculas.get(position).getUrl())
                .into(imageView);

        return vista;
    }
}
