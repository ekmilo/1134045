package moviles.sena.com.sena1134045;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class ImageActivity extends AppCompatActivity implements
        View.OnClickListener{

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        imageView=(ImageView)findViewById(R.id.image);

        Button button1=(Button)findViewById(R.id.button1);
        Button button2=(Button)findViewById(R.id.button2);

        button1.setOnClickListener(this);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Picasso.with(ImageActivity.this).load(R.drawable.angrybirds).rotate(180).into(imageView);

            }
        });

       button2.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Picasso.with(ImageActivity.this)
                       .load("https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSVOx9STiDRXsbKlnDo-1Yp1hIfYBT3Ewj_OLYgn8JUYTNnX_b5")
                       .resize(300,300)
                       .rotate(45)
                       .into(imageView);
           }
       });


    }

    @Override
    public void onClick(View v) {
        Picasso.with(ImageActivity.this).load(R.drawable.angrybirds).rotate(180).into(imageView);

    }


}
