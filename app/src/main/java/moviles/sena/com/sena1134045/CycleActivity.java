package moviles.sena.com.sena1134045;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class CycleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cycle);

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("CICLO","Estoy en pause");
    }
}
