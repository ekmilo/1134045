package moviles.sena.com.sena1134045.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ekmil on 16/5/2016.
 */
public class Movie implements Parcelable{

    private String name;
    private String url;

    public Movie(String name, String url) {
        this.name = name;
        this.url = url;
    }

    protected Movie(Parcel in) {
        name = in.readString();
        url = in.readString();
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public static List<Movie> obtenerListaPeliculas(){
        List<Movie> lista=new ArrayList<>();
        Movie soyleyenda = new Movie("soy leyenda","https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcRQB9_I_eOiS1rF9spZ8K2xFRqqet3nOL0S_LshsP9W6ZZDNGx8pQ");
        Movie guerragalaxias = new Movie("Guerra de las galaxias", "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcShBf4LNzYbQS7O9XIRYn1KsWwRSJUWViWSu1r0Qk8eT5hEyaEYyA");
        lista.add(soyleyenda);lista.add(guerragalaxias);
        lista.add(soyleyenda);lista.add(guerragalaxias);
        lista.add(soyleyenda);lista.add(guerragalaxias);
        lista.add(soyleyenda);lista.add(guerragalaxias);

        return lista;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(url);
    }
}
