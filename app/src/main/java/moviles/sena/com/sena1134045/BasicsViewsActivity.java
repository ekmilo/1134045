package moviles.sena.com.sena1134045;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class BasicsViewsActivity extends AppCompatActivity {

    TextView texto;
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basics_views);

        texto =(TextView) findViewById(R.id.texto);
        texto.setText("CHAO MUNDO");

        editText = (EditText)findViewById(R.id.edittexto);

    }

    public void hacerClick(View view){
        texto.setText( editText.getText().toString());
        Log.d("VIEW", "hizo click");
    }

}
